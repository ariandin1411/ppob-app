webpackJsonp([4],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__deposit_deposit__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__deposit_deposit__["a" /* DepositPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
        if (!localStorage.getItem('user_id')) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
        }
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\tabs\tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="tab1Root" tabTitle="List Pembayaran" tabIcon="home"></ion-tab>\n    <ion-tab [root]="tab2Root" tabTitle="Deposit" tabIcon="information-circle"></ion-tab>\n    <ion-tab [root]="tab3Root" tabTitle="Profil" tabIcon="contacts"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["h" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LupapasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LupapasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LupapasswordPage = /** @class */ (function () {
    function LupapasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LupapasswordPage.prototype.login = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
    };
    LupapasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LupapasswordPage');
    };
    LupapasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-lupapassword',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\lupapassword\lupapassword.html"*/'<!--\n  Generated template for the LupapasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-content fullscreen padding>\n    <div padding text-center>\n        <div class="logo"></div>\n        <h2 ion-text class="text-primary">\n            <strong>LUPA PASSWORD</strong>\n        </h2>\n    </div>\n    <ion-card>\n        <ion-card-content>\n            <p>Pemulihan password telah dikirmkan ke email Anda.</p>\n            <p></p>\n            <p></p>\n            <div text-center margin-top>\n                <span ion-text color="secondary" tappable (click)="login()">Login</span>\n            </div>\n        </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\lupapassword\lupapassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]])
    ], LupapasswordPage);
    return LupapasswordPage;
}());

//# sourceMappingURL=lupapassword.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DepositPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_crypto_js__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_crypto_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DepositPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DepositPage = /** @class */ (function () {
    function DepositPage(http, alertCtrl, auth, navCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.navCtrl = navCtrl;
        this.user_id = localStorage.getItem('user_id');
        console.log('user_id:', this.user_id);
        this.cekSaldo();
        this.cekVA();
    }
    DepositPage.prototype.logout = function () {
        localStorage.removeItem('user_id');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
    };
    DepositPage.prototype.sha256 = function (data) {
        var result = __WEBPACK_IMPORTED_MODULE_4_crypto_js___default.a.SHA256(data).toString(__WEBPACK_IMPORTED_MODULE_4_crypto_js___default.a.enc.Hex);
        return result;
    };
    DepositPage.prototype.konfirm = function (judul, pesan, tombol) {
        var alert = this.alertCtrl.create({
            title: judul,
            subTitle: pesan,
            buttons: tombol
        });
        alert.present();
    };
    DepositPage.prototype.cekSaldo = function () {
        var _this = this;
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_3_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        var formdata = {
            user_id: this.user_id,
            timeStamp: this.timeStamp,
            appName: this.appName,
            merchantToken: this.merchantToken
        };
        this.auth.post('users/get-balance', formdata).then(function (result) {
            console.log(result);
            if (result['msg'] == "Success") {
                _this.saldo = result['data'].balance_total;
            }
            else {
                alert('Tidak dapat terhubung dengan server');
            }
        });
    };
    DepositPage.prototype.cekVA = function () {
        var _this = this;
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_3_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        var formdata = {
            user_id: this.user_id,
            timeStamp: this.timeStamp,
            appName: this.appName,
            merchantToken: this.merchantToken
        };
        this.auth.post('users/user-va?user_id=' + this.user_id, formdata).then(function (result) {
            console.log('cek VA', result);
            if (result['data'].length > 0) {
                console.log('Sudah punya VA');
                _this.lihatVA();
            }
            else {
                _this.divVA = false;
                console.log('Belum punya VA');
            }
        });
    };
    DepositPage.prototype.lihatVA = function () {
        var _this = this;
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_3_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        var formdata = {
            user_id: this.user_id,
            timeStamp: this.timeStamp,
            appName: this.appName,
            merchantToken: this.merchantToken
        };
        this.auth.post('users/user-va', formdata).then(function (result) {
            console.log('lihat VA', result);
            if (result['data'].length > 0) {
                _this.divVA = true;
                _this.dataVA = result['data'];
                console.log('data VA', _this.dataVA);
            }
        });
    };
    DepositPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DepositPage');
    };
    DepositPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-deposit',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\deposit\deposit.html"*/'<!--\n  Generated template for the DepositPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title>\n            Deposit\n            <ion-icon name="log-out" (click)="logout()" style="float:right"></ion-icon>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding style="background-image: url(\'assets/imgs/bg.png\');background-size: cover">\n    <!-- Check saldo -->\n    <ion-card>\n        <ion-card-content>\n            <p align=right>Saldo : {{ saldo }}</p>\n        </ion-card-content>\n    </ion-card>\n\n    <!-- Buat Virtual Account -->\n    <ion-card *ngIf="divVA==false">\n        <ion-card-content>\n            <p>Vitual Account Deposit</p>\n            <h3>Halaman page-deposit</h3>\n            <p>Tekan tombol "Buat Virtual Account" untuk melakukan deosit di aplikasi kami</p>\n            <button ion-button block color="primary" (click)="cekVA()">Buat Virtual Account</button>\n        </ion-card-content>\n    </ion-card>\n\n    <!-- Buat Virtual Account -->\n    <ion-card *ngIf="divVA==true">\n        <ion-card-content>\n            <p><b>List Bank Virtual Account Anda</b></p>\n            <p>Anda bisa transfer ke salah satu nomor rekening berikut:</p>\n\n            <ion-item-group *ngFor="let item of dataVA">\n                <ion-item>\n                    <p>Bank : {{item.bank_name}}</p>\n                    <p>Nomor VA : {{item.va_no}}</p>\n                </ion-item>\n            </ion-item-group>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\deposit\deposit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], DepositPage);
    return DepositPage;
}());

//# sourceMappingURL=deposit.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_crypto_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, alertCtrl, auth, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.navParams = navParams;
    }
    RegisterPage.prototype.konfirm = function (judul, pesan, tombol) {
        var alert = this.alertCtrl.create({
            title: judul,
            subTitle: pesan,
            buttons: tombol
        });
        alert.present();
    };
    RegisterPage.prototype.sha256 = function (data) {
        var result = __WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.SHA256(data).toString(__WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.enc.Hex);
        return result;
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        if (!this.name) {
            alert('Silakan isi nama Anda!');
            return false;
        }
        if (!this.email) {
            alert('Silakan isi email Anda!');
            return false;
        }
        if (!this.phone_no) {
            alert('Silakan isi nomor handphone Anda!');
            return false;
        }
        if (!this.identity_no || this.identity_no.length != 16) {
            alert('Periksa kembali nomor identitas Anda!');
            return false;
        }
        if (this.password.length < 6) {
            alert('Password minimal 6 karakter!');
            return false;
        }
        if (this.password != this.password2) {
            alert('Password tidak sama.');
            return false;
        }
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_3_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        console.log(this.timeStamp);
        console.log(this.appName);
        console.log(this.secretCode);
        console.log(this.merchantToken);
        console.log(this.email);
        console.log(this.name);
        console.log(this.phone_no);
        console.log(this.identity_no);
        console.log(this.password);
        var formdata = {
            timeStamp: this.timeStamp,
            appName: this.appName,
            secretCode: this.secretCode,
            merchantToken: this.merchantToken,
            email: this.email,
            name: this.name,
            phone_no: this.phone_no,
            identity_no: this.identity_no,
            password: this.password
        };
        this.auth.post('users/register', formdata).then(function (result) {
            console.log(result);
            if (result['msg'] == "Success") {
                var jdl = "Berhasil";
                var msg = "Selamat, data anda telah tersimpan. ";
                var btn = [
                    {
                        text: 'OK',
                        handler: function (data) {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
                        }
                    }
                ];
                _this.konfirm(jdl, msg, btn);
            }
            else {
                var jdl = "Gagal";
                var msg = "Cek kembali data anda, dan ulangi sekali lagi! ";
                _this.konfirm(jdl, msg, ['OK']);
            }
        });
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\register\register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content fullscreen padding class="bg animated fadeInLeft">\n    <div padding text-center >\n        <div padding-horizontal text-center style="margin-top:10%">\n            <div class="logo"><img src="assets/imgs/twit.png" width=120 height=120></div>\n            <h2 ion-text class="text-primary">\n                <strong>TWITS</strong>\n            </h2>\n        </div>\n    </div>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="person" item-start class="text-primary"></ion-icon>\n            Nama lengkap\n        </ion-label>\n        <ion-input type="text" [(ngModel)]="name"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="mail" item-start class="text-primary"></ion-icon>\n            Email\n        </ion-label>\n        <ion-input type="email" [(ngModel)]="email"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="card" item-start class="text-primary"></ion-icon>\n            Nomor Identitas\n        </ion-label>\n        <ion-input type="text" [(ngModel)]="identity_no"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="call" item-start class="text-primary"></ion-icon>\n            Nomor handphone\n        </ion-label>\n        <ion-input type="text" [(ngModel)]="phone_no"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n            Password\n        </ion-label>\n        <ion-input type="password" [(ngModel)]="password"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>\n            <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n            Ulangi password\n        </ion-label>\n        <ion-input type="password" [(ngModel)]="password2"></ion-input>\n    </ion-item>\n\n\n\n    <div margin-top>\n        <button ion-button block color="secondary" tappable (click)="register()">\n                <ion-icon name="person-add"></ion-icon> Register\n        </button>\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 176:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 176;

/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/deposit/deposit.module": [
		845,
		3
	],
	"../pages/login/login.module": [
		846,
		2
	],
	"../pages/lupapassword/lupapassword.module": [
		847,
		1
	],
	"../pages/register/register.module": [
		848,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 222;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabs_tabs__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lupapassword_lupapassword__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_crypto_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, alertCtrl, http, auth, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.auth = auth;
        this.navParams = navParams;
    }
    LoginPage.prototype.hideShowPassword = function () {
        this.passwordType = !this.passwordType;
    };
    LoginPage.prototype.konfirm = function (judul, pesan, tombol) {
        var alert = this.alertCtrl.create({
            title: judul,
            subTitle: pesan,
            buttons: tombol
        });
        alert.present();
    };
    LoginPage.prototype.sha256 = function (data) {
        var result = __WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.SHA256(data).toString(__WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.enc.Hex);
        return result;
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (this.email == '' || this.password == '') {
            this.konfirm("Peringatan!", "Cek username atau password anda!", ['OK']);
            return false;
        }
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_7_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        console.log(this.timeStamp);
        console.log(this.appName);
        console.log(this.secretCode);
        console.log(this.merchantToken);
        console.log(this.email);
        console.log(this.password);
        var formdata = {
            timeStamp: this.timeStamp,
            appName: this.appName,
            secretCode: this.secretCode,
            merchantToken: this.merchantToken,
            email: this.email,
            password: this.password
        };
        this.auth.post('users/login', formdata).then(function (result) {
            console.log(result);
            if (result['msg'] == "Success") {
                var jdl = "Berhasil";
                var msg = "Selamat datang, " + result['data'].name;
                var btn = [
                    {
                        text: 'OK',
                        handler: function (data) {
                            localStorage.setItem('user_id', result['data'].id);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__tabs_tabs__["a" /* TabsPage */]);
                        }
                    }
                ];
                _this.konfirm(jdl, msg, btn);
            }
            else {
                var jdl = "Gagal";
                var msg = "Cek kembali username dan password anda! ";
                _this.konfirm(jdl, msg, ['OK']);
            }
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.lupaPassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__lupapassword_lupapassword__["a" /* LupapasswordPage */]);
    };
    LoginPage.prototype.pageReload = function () {
        this.forceReload();
    };
    LoginPage.prototype.forceReload = function (refresher) {
        var page = 1;
        console.log("reload:", refresher);
        setTimeout(function () {
            refresher && refresher.complete();
        }, 2000);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\login\login.html"*/'<!--style="background-image: url(\'assets/imgs/bg\');background-size: cover"\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content fullscreen padding class="bg">\n    <div>\n        <div padding-horizontal text-center style="margin-top:10%">\n            <div><img src="assets/imgs/twit.png" width=120 height=120></div>\n            <h2 ion-text class="text-primary">\n                <strong>TWITS</strong>\n            </h2>\n        </div>\n        <ion-item>\n            <ion-label floating>\n                <ion-icon name="mail" item-start class="text-primary"></ion-icon>\n                Email\n            </ion-label>\n            <ion-input type="email" [(ngModel)]="email"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>\n                <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n                Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password"></ion-input>\n        </ion-item>\n\n\n        <div>\n        </div>\n\n\n\n        <ion-grid>\n            <ion-row>\n                <ion-col col-6 >\n                    <button ion-button icon-sign color="secondary" tappable (click)="register()" block>\n                        <ion-icon name="person-add"></ion-icon> &nbsp;Register\n                    </button>\n                </ion-col>\n                <ion-col col-6>\n                    <button ion-button icon-start color="primary" tappable (click)="login()" block>\n                        <ion-icon name="log-in"></ion-icon>Login\n                    </button>\n                </ion-col>\n            </ion-row>\n\n        </ion-grid>\n        <p text-center ion-text color="danger" tappable (click)="lupaPassword()" warning>Lupa password?</p>\n\n        <button ion-button icon-start color="secondary" tappable (click)=" pageReload()" block>\n          <ion-icon name="log-in"></ion-icon>reload\n      </button>\n\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\contact\contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\contact\contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 485:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_crypto_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = /** @class */ (function () {
    function HomePage(http, alertCtrl, auth, navCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.navCtrl = navCtrl;
        if (localStorage.getItem('user_id')) {
            this.user_id = localStorage.getItem('user_id');
            console.log('user_id:', this.user_id);
            this.cekSaldo();
        }
    }
    HomePage.prototype.logout = function () {
        localStorage.removeItem('user_id');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.sha256 = function (data) {
        var result = __WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.SHA256(data).toString(__WEBPACK_IMPORTED_MODULE_5_crypto_js___default.a.enc.Hex);
        return result;
    };
    HomePage.prototype.konfirm = function (judul, pesan, tombol) {
        var alert = this.alertCtrl.create({
            title: judul,
            subTitle: pesan,
            buttons: tombol
        });
        alert.present();
    };
    HomePage.prototype.cekSaldo = function () {
        var _this = this;
        /*
        let postData = {
          "user_id" : userData.id,
          "timeStamp" : this.envService.API_TIMESTAMP,
          "appName" : this.envService.API_NAME,
          "merchantToken" : this.envService.API_TOKEN
          }
          this.httpClient.post(this.envService.API_URL+"users/get-balance", postData, options)
    */
        this.timeStamp = __WEBPACK_IMPORTED_MODULE_4_moment___default()(Date()).format('YYYYMMDDhmmss');
        this.appName = "twits-ppob";
        this.secretCode = "rahasiaBgt123";
        var mToken = this.timeStamp + this.appName + this.secretCode;
        this.merchantToken = this.sha256(mToken);
        var formdata = {
            user_id: this.user_id,
            timeStamp: this.timeStamp,
            appName: this.appName,
            merchantToken: this.merchantToken
        };
        this.auth.post('users/get-balance', formdata).then(function (result) {
            console.log(result);
            if (result['msg'] == "Success") {
                _this.saldo = result['data'].balance_total;
            }
            else {
                alert('Tidak dapat terhubung dengan server');
            }
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\home\home.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            Home\n            <ion-icon name="log-out" (click)="logout()" style="float:right"></ion-icon>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding style="background-image: url(\'assets/imgs/bg.png\');background-size: cover">\n    <!-- Check saldo -->\n    <ion-card>\n        <ion-card-content>\n            <p align=right>Saldo : {{ saldo }}</p>\n        </ion-card-content>\n    </ion-card>\n\n    <!-- Daftar produk -->\n    <ion-card>\n        <ion-card-header>\n            PULSA\n        </ion-card-header>\n        <ion-card-content>\n            <ion-grid>\n                <ion-row>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/pulsa.jpg" alt="Paket Pulsa"> Paket Pulsa\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/paketdata.jpg" alt="Paket Data"> Paket Data\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/pulsapasca.jpg" alt="Pasca bayar"> Pasca bayar\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <ion-card-header>\n            PEMBAYARAN\n        </ion-card-header>\n        <ion-card-content>\n            <ion-grid>\n                <ion-row>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/listrik.jpg" alt="Tagihan Listrik"> Tagihan Listrik\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/pam.jpg" alt="Tagihan Air"> Tagihan Air\n                        </div>\n\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/kredit.jpg" alt="Angsuran Kredit"> Angsuran Kredit\n                        </div>\n                    </ion-col>\n                </ion-row>\n\n                <ion-row>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/zakat.jpg" alt="Bayar Zakat"> Bayar Zakat\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/emoney.jpg" alt="Uang Elektronik"> Uang Elektronik\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/game.jpg" alt="Voucher Game"> Voucher Game\n                        </div>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/telkom.jpg" alt="Telkom"> Telkom\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/tvkabel.jpg" alt="TV Kabel"> TV\n                        </div>\n                    </ion-col>\n                    <ion-col size="4">\n                        <div class="produk">\n                            <img src="assets/imgs/gambar/bpjs.jpg" alt="BPJS"> BPJS\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(495);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_lupapassword_lupapassword__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_register_register__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(840);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about_about__ = __webpack_require__(844);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_contact_contact__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_auth_auth__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_deposit_deposit__ = __webpack_require__(164);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_lupapassword_lupapassword__["a" /* LupapasswordPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_deposit_deposit__["a" /* DepositPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */], __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/deposit/deposit.module#DepositPageModule', name: 'DepositPage', segment: 'deposit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lupapassword/lupapassword.module#LupapasswordPageModule', name: 'LupapasswordPage', segment: 'lupapassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_lupapassword_lupapassword__["a" /* LupapasswordPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_deposit_deposit__["a" /* DepositPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_15__providers_auth_auth__["a" /* AuthProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["d" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 223,
	"./af.js": 223,
	"./ar": 224,
	"./ar-dz": 225,
	"./ar-dz.js": 225,
	"./ar-kw": 226,
	"./ar-kw.js": 226,
	"./ar-ly": 227,
	"./ar-ly.js": 227,
	"./ar-ma": 228,
	"./ar-ma.js": 228,
	"./ar-sa": 229,
	"./ar-sa.js": 229,
	"./ar-tn": 230,
	"./ar-tn.js": 230,
	"./ar.js": 224,
	"./az": 231,
	"./az.js": 231,
	"./be": 232,
	"./be.js": 232,
	"./bg": 233,
	"./bg.js": 233,
	"./bm": 234,
	"./bm.js": 234,
	"./bn": 235,
	"./bn.js": 235,
	"./bo": 236,
	"./bo.js": 236,
	"./br": 237,
	"./br.js": 237,
	"./bs": 238,
	"./bs.js": 238,
	"./ca": 239,
	"./ca.js": 239,
	"./cs": 240,
	"./cs.js": 240,
	"./cv": 241,
	"./cv.js": 241,
	"./cy": 242,
	"./cy.js": 242,
	"./da": 243,
	"./da.js": 243,
	"./de": 244,
	"./de-at": 245,
	"./de-at.js": 245,
	"./de-ch": 246,
	"./de-ch.js": 246,
	"./de.js": 244,
	"./dv": 247,
	"./dv.js": 247,
	"./el": 248,
	"./el.js": 248,
	"./en-SG": 249,
	"./en-SG.js": 249,
	"./en-au": 250,
	"./en-au.js": 250,
	"./en-ca": 251,
	"./en-ca.js": 251,
	"./en-gb": 252,
	"./en-gb.js": 252,
	"./en-ie": 253,
	"./en-ie.js": 253,
	"./en-il": 254,
	"./en-il.js": 254,
	"./en-nz": 255,
	"./en-nz.js": 255,
	"./eo": 256,
	"./eo.js": 256,
	"./es": 257,
	"./es-do": 258,
	"./es-do.js": 258,
	"./es-us": 259,
	"./es-us.js": 259,
	"./es.js": 257,
	"./et": 260,
	"./et.js": 260,
	"./eu": 261,
	"./eu.js": 261,
	"./fa": 262,
	"./fa.js": 262,
	"./fi": 263,
	"./fi.js": 263,
	"./fo": 264,
	"./fo.js": 264,
	"./fr": 265,
	"./fr-ca": 266,
	"./fr-ca.js": 266,
	"./fr-ch": 267,
	"./fr-ch.js": 267,
	"./fr.js": 265,
	"./fy": 268,
	"./fy.js": 268,
	"./ga": 269,
	"./ga.js": 269,
	"./gd": 270,
	"./gd.js": 270,
	"./gl": 271,
	"./gl.js": 271,
	"./gom-latn": 272,
	"./gom-latn.js": 272,
	"./gu": 273,
	"./gu.js": 273,
	"./he": 274,
	"./he.js": 274,
	"./hi": 275,
	"./hi.js": 275,
	"./hr": 276,
	"./hr.js": 276,
	"./hu": 277,
	"./hu.js": 277,
	"./hy-am": 278,
	"./hy-am.js": 278,
	"./id": 279,
	"./id.js": 279,
	"./is": 280,
	"./is.js": 280,
	"./it": 281,
	"./it-ch": 282,
	"./it-ch.js": 282,
	"./it.js": 281,
	"./ja": 283,
	"./ja.js": 283,
	"./jv": 284,
	"./jv.js": 284,
	"./ka": 285,
	"./ka.js": 285,
	"./kk": 286,
	"./kk.js": 286,
	"./km": 287,
	"./km.js": 287,
	"./kn": 288,
	"./kn.js": 288,
	"./ko": 289,
	"./ko.js": 289,
	"./ku": 290,
	"./ku.js": 290,
	"./ky": 291,
	"./ky.js": 291,
	"./lb": 292,
	"./lb.js": 292,
	"./lo": 293,
	"./lo.js": 293,
	"./lt": 294,
	"./lt.js": 294,
	"./lv": 295,
	"./lv.js": 295,
	"./me": 296,
	"./me.js": 296,
	"./mi": 297,
	"./mi.js": 297,
	"./mk": 298,
	"./mk.js": 298,
	"./ml": 299,
	"./ml.js": 299,
	"./mn": 300,
	"./mn.js": 300,
	"./mr": 301,
	"./mr.js": 301,
	"./ms": 302,
	"./ms-my": 303,
	"./ms-my.js": 303,
	"./ms.js": 302,
	"./mt": 304,
	"./mt.js": 304,
	"./my": 305,
	"./my.js": 305,
	"./nb": 306,
	"./nb.js": 306,
	"./ne": 307,
	"./ne.js": 307,
	"./nl": 308,
	"./nl-be": 309,
	"./nl-be.js": 309,
	"./nl.js": 308,
	"./nn": 310,
	"./nn.js": 310,
	"./pa-in": 311,
	"./pa-in.js": 311,
	"./pl": 312,
	"./pl.js": 312,
	"./pt": 313,
	"./pt-br": 314,
	"./pt-br.js": 314,
	"./pt.js": 313,
	"./ro": 315,
	"./ro.js": 315,
	"./ru": 316,
	"./ru.js": 316,
	"./sd": 317,
	"./sd.js": 317,
	"./se": 318,
	"./se.js": 318,
	"./si": 319,
	"./si.js": 319,
	"./sk": 320,
	"./sk.js": 320,
	"./sl": 321,
	"./sl.js": 321,
	"./sq": 322,
	"./sq.js": 322,
	"./sr": 323,
	"./sr-cyrl": 324,
	"./sr-cyrl.js": 324,
	"./sr.js": 323,
	"./ss": 325,
	"./ss.js": 325,
	"./sv": 326,
	"./sv.js": 326,
	"./sw": 327,
	"./sw.js": 327,
	"./ta": 328,
	"./ta.js": 328,
	"./te": 329,
	"./te.js": 329,
	"./tet": 330,
	"./tet.js": 330,
	"./tg": 331,
	"./tg.js": 331,
	"./th": 332,
	"./th.js": 332,
	"./tl-ph": 333,
	"./tl-ph.js": 333,
	"./tlh": 334,
	"./tlh.js": 334,
	"./tr": 335,
	"./tr.js": 335,
	"./tzl": 336,
	"./tzl.js": 336,
	"./tzm": 337,
	"./tzm-latn": 338,
	"./tzm-latn.js": 338,
	"./tzm.js": 337,
	"./ug-cn": 339,
	"./ug-cn.js": 339,
	"./uk": 340,
	"./uk.js": 340,
	"./ur": 341,
	"./ur.js": 341,
	"./uz": 342,
	"./uz-latn": 343,
	"./uz-latn.js": 343,
	"./uz.js": 342,
	"./vi": 344,
	"./vi.js": 344,
	"./x-pseudo": 345,
	"./x-pseudo.js": 345,
	"./yo": 346,
	"./yo.js": 346,
	"./zh-cn": 347,
	"./zh-cn.js": 347,
	"./zh-hk": 348,
	"./zh-hk.js": 348,
	"./zh-tw": 349,
	"./zh-tw.js": 349
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 516;

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_login_login__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AuthProvider = /** @class */ (function () {
    function AuthProvider(app, http, loadingCtrl, toastCtrl, alertCtrl) {
        this.app = app;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.apiUrl = 'http://localhost/twits-ppob/';
        this.apiUrl = 'http://twits-ppob.my.id/api/android/';
    }
    AuthProvider.prototype.loadingShow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.loading) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                spinner: 'dots',
                                content: 'Proses membaca... ',
                                duration: 3000,
                                showBackdrop: true,
                                enableBackdropDismiss: true
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthProvider.prototype.dismissLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.loading) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.loading.dismiss()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    AuthProvider.prototype.post = function (url, post, load) {
        var _this = this;
        if (load === void 0) { load = ''; }
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        this.loadingShow();
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + url, (post), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
                _this.dismissLoading();
            }, function (err) {
                reject(err);
                _this.dismissLoading();
            });
        });
    };
    AuthProvider.prototype.registerUser = function (formdata) {
        return this.post('users/register', formdata);
    };
    AuthProvider.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    AuthProvider.prototype.logout = function () {
        localStorage.clear();
        this.app.getActiveNav().push(__WEBPACK_IMPORTED_MODULE_0__pages_login_login__["a" /* LoginPage */]);
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    //rootPage:any = HomePage;
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\about\about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Ariandi\Documents\ionic\twits-ppob-new\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ })

},[490]);
//# sourceMappingURL=main.js.map
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';
import { AuthProvider } from '../../providers/auth/auth';
import CryptoJS from 'crypto-js';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  password:string;
  password2:string;
  name:string;
  phone_no:string;
  identity_no:string;
  data: Observable<any>;
  timeStamp:string;
  appName:string;
  secretCode:string;
  merchantToken:string;
  email:string;
  username:string;

  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController,
    public auth: AuthProvider,
    public navParams: NavParams) {
  }

  konfirm(judul,pesan,tombol){
    const alert = this.alertCtrl.create({
      title: judul,
      subTitle: pesan,
      buttons: tombol
    });
    alert.present();
  }

  sha256(data){
    let result = CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
    return result;
  }
  register(){
    if(!this.name){
      alert('Silakan isi nama Anda!');
      return false;
    }
    if(!this.email){
      alert('Silakan isi email Anda!');
      return false;
    }
    if(!this.phone_no){
      alert('Silakan isi nomor handphone Anda!');
      return false;
    }
    if(!this.identity_no || this.identity_no.length !=16){
      alert('Periksa kembali nomor identitas Anda!');
      return false;
    }
    if(this.password.length<6){
      alert('Password minimal 6 karakter!');
      return false;
    }
    if(this.password!=this.password2 ){
      alert('Password tidak sama.');
      return false;
    }
    
    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    console.log(this.timeStamp);
    console.log(this.appName);
    console.log(this.secretCode);
    console.log(this.merchantToken);
    console.log(this.email);
    console.log(this.name);
    console.log(this.phone_no);
    console.log(this.identity_no);
    console.log(this.password);

    let formdata = {
      timeStamp:this.timeStamp,
      appName:this.appName,
      secretCode:this.secretCode,
      merchantToken:this.merchantToken,
      email:this.email,
      name:this.name,
      phone_no:this.phone_no,
      identity_no:this.identity_no,
      password:this.password
    }; 
    
    this.auth.post('users/register', formdata).then((result: any[]) => {
      console.log(result);
      if(result['msg']=="Success"){
        let jdl="Berhasil";
        let msg="Selamat, data anda telah tersimpan. ";
        let btn =[
          {
            text: 'OK',
            handler: data => {
              this.navCtrl.setRoot(LoginPage);
            }
          }];
        this.konfirm(jdl,msg,btn);
      }else{
        let jdl="Gagal";
        let msg="Cek kembali data anda, dan ulangi sekali lagi! ";
        this.konfirm(jdl,msg,['OK']);
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}

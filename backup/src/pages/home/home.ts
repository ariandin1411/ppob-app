import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  saldo:string;
  user_id:string;
  timeStamp:string;
  appName:string;
  secretCode:string;
  merchantToken:string;
  data: Observable<any>;

  constructor(
    public http:HttpClient,
    public alertCtrl: AlertController,
    public auth: AuthProvider,
    public navCtrl: NavController
    ) {

      if(localStorage.getItem('user_id')){
        this.user_id=localStorage.getItem('user_id');
        console.log('user_id:',this.user_id);
        this.cekSaldo();
      }

  }
  logout(){
    localStorage.removeItem('user_id');
    this.navCtrl.setRoot(LoginPage);
  }
  sha256(data){
    let result = CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
    return result;
  }
  konfirm(judul,pesan,tombol){
    const alert = this.alertCtrl.create({
      title: judul,
      subTitle: pesan,
      buttons: tombol
    });
    alert.present();
  }

  cekSaldo(){
    /*
    let postData = {
      "user_id" : userData.id,
      "timeStamp" : this.envService.API_TIMESTAMP,
      "appName" : this.envService.API_NAME,
      "merchantToken" : this.envService.API_TOKEN
      }
      this.httpClient.post(this.envService.API_URL+"users/get-balance", postData, options)
*/
    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    let formdata = {
      user_id:this.user_id,
      timeStamp:this.timeStamp,
      appName:this.appName,
      merchantToken:this.merchantToken
    };

    this.auth.post('users/get-balance', formdata).then((result: any[]) => {
      console.log(result);
      if(result['msg']=="Success"){
        this.saldo=result['data'].balance_total;
      }else{
        alert('Tidak dapat terhubung dengan server');
      }

    });

  }

}

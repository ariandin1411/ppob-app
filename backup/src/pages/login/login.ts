import { TabsPage } from './../tabs/tabs';
import { LupapasswordPage } from './../lupapassword/lupapassword';
import { RegisterPage } from './../register/register';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import CryptoJS from 'crypto-js';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: any;
  passwordType: boolean;
  data: Observable<any>;
  timeStamp:string;
  appName:string;
  secretCode:string;
  merchantToken:string;
  email:string;
  password:string;
  username:string;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public http:HttpClient,
    public auth: AuthProvider,
    public navParams: NavParams) {
  }

  hideShowPassword(){
    this.passwordType = !this.passwordType;
  }

  konfirm(judul,pesan,tombol){
    const alert = this.alertCtrl.create({
      title: judul,
      subTitle: pesan,
      buttons: tombol
    });
    alert.present();
  }

  sha256(data){
    let result = CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
    return result;
  }


  login(){

    if (this.email == '' || this.password == '') {
      this.konfirm("Peringatan!","Cek username atau password anda!",['OK']);
      return false;
    }

    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    console.log(this.timeStamp);
    console.log(this.appName);
    console.log(this.secretCode);
    console.log(this.merchantToken);
    console.log(this.email);
    console.log(this.password);


    let formdata = {
      timeStamp:this.timeStamp,
      appName:this.appName,
      secretCode:this.secretCode,
      merchantToken:this.merchantToken,
      email:this.email,
      password:this.password
    };

    this.auth.post('users/login', formdata).then((result: any[]) => {
        console.log(result);
        if(result['msg']=="Success"){
          let jdl="Berhasil";
          let msg="Selamat datang, "+result['data'].name;
          let btn =[
            {
              text: 'OK',
              handler: data => {
                localStorage.setItem('user_id',result['data'].id);
                this.navCtrl.setRoot(TabsPage);
              }
            }];
          this.konfirm(jdl,msg,btn);
        }else{
          let jdl="Gagal";
          let msg="Cek kembali username dan password anda! ";
          this.konfirm(jdl,msg,['OK']);
        }
    })
    .catch((e) => {
      alert(JSON.stringify(e));
    });
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  lupaPassword(){
    this.navCtrl.push(LupapasswordPage);
  }

  pageReload(){
    this.forceReload();
  }

  forceReload(refresher?:any) {
    let page = 1;
    console.log("reload:",refresher)
    setTimeout(() => {
      refresher && refresher.complete();
    }, 2000);
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}

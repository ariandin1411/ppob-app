import { LoginPage } from './../../pages/login/login';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { App, LoadingController, AlertController, ToastController } from 'ionic-angular';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  apiUrl = 'http://localhost/twits-ppob/';
  loading : any;
  loadingBool : boolean = false;

  constructor(    
    public app: App,
    public http: Http,
    public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
    ) {
    this.apiUrl ='http://twits-ppob.my.id/api/android/';
  }


  loadingShow(){
    if(!this.loadingBool){
      this.loading = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Proses membaca... ',
        // duration: 3000,
        showBackdrop: true,
        enableBackdropDismiss: true
      });
      // return await loading.present();
      this.loading.present();
      this.loadingBool = true;

    }

  }

  dismissLoading(){
    if(this.loadingBool){
      this.loading.dismiss();
      this.loadingBool = false;
    }
  }

  post( url, post, load = ''){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.loadingShow();
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + url, (post), {headers: headers})
      .subscribe(res => {
        resolve(res.json());
        this.dismissLoading();
      }, (err) => {
        reject(err);
        this.dismissLoading();
      });
    });
  }

  registerUser(formdata){
    return this.post('users/register', formdata);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      // duration: 3000,
      position: 'bottom'
    });  
    toast.present();
  }

  logout() {
    localStorage.clear();
    this.app.getActiveNav().push(LoginPage);
  }


}

import { LoginPage } from './../../pages/login/login';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { App, LoadingController, AlertController, ToastController } from 'ionic-angular';


@Injectable()
export class AuthProvider {
  
  apiUrl = 'http://localhost/twits-ppob/';
  loading : any;
  
  constructor(
    public app: App,
    public http: Http,
    public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
      // 
      this.apiUrl ='http://localhost/twits-ppob/api/android/';
  }

  async loadingShow(){
    if(!this.loading){
      const loading = await this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Proses membaca... ',
        duration: 3000,
        showBackdrop: true,
        enableBackdropDismiss: true
      });
      return await loading.present();
    }
  }

  async dismissLoading(){
    if(this.loading){
      await this.loading.dismiss();
    }
  }

  post( url, post, load = ''){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.loadingShow();
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + url, (post), {headers: headers})
      .subscribe(res => {
        resolve(res.json());
        this.dismissLoading();
      }, (err) => {
        reject(err);
        this.dismissLoading();
      });
    });
  }

  userLogin(username, password){
    let formdata = {
      InputEmail: username,
      InputPassword: password
    };
    return this.post('users/login', formdata);
  }

  registerUser(formdata){
    return this.post('users/register', formdata);
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });  
    toast.present();
  }

  logout() {
    localStorage.clear();
    this.app.getActiveNav().push(LoginPage);
  }

}

import { Component } from '@angular/core';

import { DepositPage } from '../deposit/deposit';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { NavController,AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = DepositPage;
  tab3Root = ContactPage;

  constructor(public navCtrl: NavController) {
    if(!localStorage.getItem('user_id')){
      this.navCtrl.setRoot(LoginPage);
    }

  }
}

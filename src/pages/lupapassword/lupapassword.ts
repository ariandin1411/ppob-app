import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LupapasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lupapassword',
  templateUrl: 'lupapassword.html',
})
export class LupapasswordPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  login(){
      this.navCtrl.setRoot(LoginPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LupapasswordPage');
  }

}

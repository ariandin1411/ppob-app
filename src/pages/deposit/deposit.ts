import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';


/**
 * Generated class for the DepositPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deposit',
  templateUrl: 'deposit.html',
})
export class DepositPage {
  saldo:string;
  user_id:string;
  timeStamp:string;
  appName:string;
  secretCode:string;
  merchantToken:string;
  data: Observable<any>;
  divVA:boolean;
  dataVA: Observable<any>;

  constructor(
    public http:HttpClient,
    public alertCtrl: AlertController,
    public auth: AuthProvider,
    public navCtrl: NavController) {

      this.user_id=localStorage.getItem('user_id');
      console.log('user_id:',this.user_id);
      this.cekSaldo();
  }

  logout(){
    localStorage.removeItem('user_id');
    this.navCtrl.setRoot(LoginPage);
  }
  sha256(data){
    let result = CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
    return result;
  }
  konfirm(judul,pesan,tombol){
    const alert = this.alertCtrl.create({
      title: judul,
      subTitle: pesan,
      buttons: tombol
    });
    alert.present();
  }

  cekSaldo(){
    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    let formdata = {
      user_id:this.user_id,
      timeStamp:this.timeStamp,
      appName:this.appName,
      merchantToken:this.merchantToken
    };

    this.auth.post('users/get-balance', formdata).then((result: any[]) => {
      console.log(result);
      if(result['msg']=="Success"){
        this.saldo=result['data'].balance_total;
        this.cekVA();
      }else{
        alert('Tidak dapat terhubung dengan server');
      }

    })
    .catch((e) => {
      alert(JSON.stringify(e))
    });

  }
  cekVA(){
    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    let formdata = {
      user_id:this.user_id,
      timeStamp:this.timeStamp,
      appName:this.appName,
      merchantToken:this.merchantToken
    };

    this.auth.post('users/user-va?user_id=' + this.user_id, formdata).then((result: any[]) => {
      console.log('cek VA',result);
      if(result['data'].length>0){
        console.log('Sudah punya VA');
        this.lihatVA();

      }else{
        this.divVA=false;
        console.log('Belum punya VA');
      }

    })
    .catch((e) => {
      alert(JSON.stringify(e))
    });

  }

  lihatVA(){
    this.timeStamp=moment(Date()).format('YYYYMMDDhmmss');
    this.appName = "twits-ppob";
    this.secretCode = "rahasiaBgt123";
    let mToken=this.timeStamp+this.appName+this.secretCode;
    this.merchantToken = this.sha256(mToken);

    let formdata = {
      user_id:this.user_id,
      timeStamp:this.timeStamp,
      appName:this.appName,
      merchantToken:this.merchantToken
    };

    this.auth.post('users/user-va', formdata).then((result: any[]) => {
      console.log('lihat VA',result);
      if(result['data'].length>0){
        this.divVA=true;
        this.dataVA=result['data'];
        console.log('data VA',this.dataVA);
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DepositPage');
  }

}
